# Hot Potato

Hot Potato is intended to sit inbetween multiple monitoring systems and your messaging providers to ensure consistent relaying of messages to on-call staff.

Contents
--------

* [Project details](project.html)
    * [Code of Conduct](code-of-conduct.html)
    * [Contribution guide](contributing.html)
    * [Information for developers](developing.html)
* [Hot Potato API documentation](api.html)
